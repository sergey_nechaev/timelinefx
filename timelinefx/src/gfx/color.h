/*
 * color.h
 *
 *  Created on: 04/03/2015
 *      Author: Sergey.Nechaev
 */

#ifndef color_H_
#define color_H_

#include <SDL2/SDL_pixels.h>
#include <string>

namespace gfx {

	class color {

		public:

			color();
			color(const int r, const int g, const int b);
			color(const int r, const int g, const int b, const int a);

			void set(const int r, const int g, const int b, const int a = 255);

			int get_red() const;
			int get_green() const;
			int get_blue() const;
			int get_alpha() const;
			int get_rgb() const;

			const SDL_Color& get_sdl_color() const;

			std::string to_string() const;

		private:
			int value = 0;
			SDL_Color sdl_color { 0, 0, 0, 0 };

	};

} /* namespace lib */

#endif
