/*
 * color.cpp
 *
 *  Created on: 10Sep.,2016
 *      Author: admin
 */

#include "color.h"

namespace gfx {

	color::color() {
	}

	color::color(const int r, const int g, const int b) :
			color(r, g, b, 255) {
	}

	color::color(const int r, const int g, const int b, const int a) {
		set(r, g, b, a);
	}

	int color::get_red() const {
		return (get_rgb() >> 16) & 0xFF;
	}

	int color::get_green() const {
		return (get_rgb() >> 8) & 0xFF;
	}

	int color::get_blue() const {
		return (get_rgb() >> 0) & 0xFF;
	}

	int color::get_alpha() const {
		return (get_rgb() >> 24) & 0xff;
	}

	int color::get_rgb() const {
		return value;
	}

	const SDL_Color& color::get_sdl_color() const {
		return this->sdl_color;
	}

	void color::set(const int r, const int g, const int b, const int a) {

		value = ((a & 0xFF) << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8) | ((b & 0xFF) << 0);
		this->sdl_color.r = r;
		this->sdl_color.g = g;
		this->sdl_color.b = b;
		this->sdl_color.a = a;
	}

	std::string color::to_string() const {
		return "color RGBA [" + std::to_string(get_red()) + ", " + std::to_string(get_green()) + ", " + std::to_string(get_blue()) + ", " + std::to_string(get_alpha()) + "]";
	}

}

