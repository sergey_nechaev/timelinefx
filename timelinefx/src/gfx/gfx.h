/*
 * gfx.h
 *
 *  Created on: 9Sep.,2016
 *      Author: Sergey.Nechaev
 */

#ifndef GFX_H_
#define GFX_H_

#include <string>

struct SDL_Renderer;
struct SDL_Surface;
struct SDL_Texture;

namespace gfx {

	SDL_Texture* to_texture(SDL_Renderer* ren, SDL_Surface* surface);
	SDL_Texture* load_texture(SDL_Renderer* ren, std::string file);
	void free_surface(SDL_Surface* surf);
}

#endif /* GFX_H_ */
