/*
 * gfx.cpp
 *
 *  Created on: 9Sep.,2016
 *      Author: Sergey.Nechaev
 */

#include "gfx.h"

#include <SDL2/SDL_error.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_surface.h>
#include <cassert>
#include <iostream>
#include <string>

void gfx::free_surface(SDL_Surface* surf) {

	assert(surf != nullptr);

	SDL_FreeSurface(surf);
	surf = nullptr;
}

SDL_Texture* gfx::to_texture(SDL_Renderer* ren, SDL_Surface* surface) {

	assert(ren != nullptr && surface != nullptr);

	SDL_Texture* texture = SDL_CreateTextureFromSurface(ren, surface);
	return texture;
}

SDL_Texture* gfx::load_texture(SDL_Renderer* ren, std::string file) {

	assert(ren != nullptr);
	assert(!file.empty());

	auto surface = IMG_Load(file.c_str());

	if (!surface) {
		std::cerr << "Error loading surface: " << SDL_GetError() << std::endl;
		std::terminate();
	}

	auto tex = gfx::to_texture(ren, surface);

	gfx::free_surface(surface);

	return tex;

}
