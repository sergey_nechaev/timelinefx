#include <SDL2/SDL.h>
#include <SDL2/SDL_error.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_main.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_timer.h>
#include <SDL2/SDL_version.h>
#include <SDL2/SDL_video.h>
#include <cstdio>
#include <exception>
#include <iostream>

#include "tlfx/SDL2EffectsLibrary.h"
#include "tlfx/TLFXEffect.h"

using namespace std;

int main(int argv, char* argc[]) {

	auto w = 1024;
	auto h = 768;

	if (SDL_Init( SDL_INIT_EVERYTHING) < 0) {
		std::cerr << "There was an error initializing SDL2: " << SDL_GetError() << std::endl;
		std::terminate();
	}

	SDL_Window* window = SDL_CreateWindow("SDL2 TimelineFX", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);

	if (window == nullptr) {
		std::cerr << "There was an error creating the window: " << SDL_GetError() << std::endl;
		std::terminate();
	}

	SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, SDL_RendererFlags::SDL_RENDERER_ACCELERATED);

	if (renderer == nullptr) {
		std::cerr << "There was an error creating the renderer: " << SDL_GetError() << std::endl;
		std::terminate();
	}

	// init SDL image

	int image_flags = IMG_INIT_JPG | IMG_INIT_PNG;
	int initted = IMG_Init(image_flags);
	if ((initted & image_flags) != image_flags) {
		std::cerr << "IMG_Init: Failed to init required jpg and png support!" << std::endl;
		std::cerr << "IMG_Init: " << IMG_GetError() << std::endl;
		std::terminate();
	}

	{
		SDL_version compile_version;
		const SDL_version *link_version = IMG_Linked_Version();
		SDL_IMAGE_VERSION(&compile_version);
		printf("SDL_Image: Compiled with SDL_image version: %d.%d.%d\n", compile_version.major, compile_version.minor, compile_version.patch);
		printf("SDL_Image: Running with SDL_image version: %d.%d.%d\n", link_version->major, link_version->minor, link_version->patch);
	}



	// tlfx
	auto gEffects = new SDL2EffectsLibrary();
	gEffects->Load(renderer, "particles/data.xml");

	auto gPM = new SDL2ParticleManager(renderer);
	gPM->SetScreenSize(w, h);
	gPM->SetOrigin(0, 0);


	{
		std::string effect_name = "Area Effects/Swirly Balls";
		auto eff = gEffects->GetEffect(effect_name.c_str());
		if (eff==nullptr) {
			std::cerr << "Cannot load effect " << effect_name << "\n";
			std::exit(EXIT_FAILURE);
		}
		auto copy = new TLFX::Effect(*eff, gPM);
		copy->SetPosition(-0.0f, -0.0f);
		gPM->AddEffect(copy);
	}

	// delta
	bool running = true;
	float deltaTime = 0;
	int lastFrameTime = 0, currentFrameTime = 0;

	SDL_Event event;

	while (running) {

		//get last frame time and reset current frame time
		lastFrameTime = currentFrameTime;
		currentFrameTime = SDL_GetTicks();

		//set delta time
		deltaTime = (float) (currentFrameTime - lastFrameTime) / 1000;

		if (SDL_PollEvent(&event)) {

			switch (event.type) {

				case SDL_QUIT:
					running = false;
					break;

				case SDL_KEYDOWN:

					switch (event.key.keysym.sym) {
						case SDLK_ESCAPE:
						case SDLK_AC_BACK:
							running = false;
							break;
					}

					break;

				default:
					break;
			}
		}

		// tlfx
		gPM->Update();

		SDL_RenderClear(renderer);

		gPM->DrawParticles(deltaTime);

		gPM->Flush();

		SDL_RenderPresent(renderer);

		SDL_Delay(4);

	}

	// tlfx
	delete gEffects;
	delete gPM;

	IMG_Quit();

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}
