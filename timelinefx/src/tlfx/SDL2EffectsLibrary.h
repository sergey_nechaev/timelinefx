#ifdef _MSC_VER
#pragma once
#endif

/*
 * sdl2 renderer API for rendering
 * PugiXML for parsing data
 */

#include <string>

struct SDL_Texture;

#include <list>

#include "../gfx/color.h"

#ifndef _MARMALADEEFFECTSLIBRARY_H
#define _MARMALADEEFFECTSLIBRARY_H

#include "TLFXEffectsLibrary.h"
#include "TLFXParticleManager.h"
#include "TLFXAnimImage.h"

class XMLLoader;

class SDL2EffectsLibrary: public TLFX::EffectsLibrary {
	public:
		virtual TLFX::XMLLoader* CreateLoader() const;
		virtual TLFX::AnimImage* CreateImage() const;
};

class SDL2ParticleManager: public TLFX::ParticleManager {

	public:

		SDL2ParticleManager(SDL_Renderer* ren, int particles = TLFX::ParticleManager::particleLimit, int layers = 1);
		void Flush();

	protected:
		virtual void DrawSprite(TLFX::AnimImage* sprite, float px, float py, float frame, float x, float y, float rotation, float scaleX, float scaleY, unsigned char r, unsigned char g, unsigned char b, float a, bool additive);

		// batching
		struct Batch {
				float px, py;
				float frame;
				float x, y;
				float rotation;
				float scaleX, scaleY;
				int w,h;
				int frame_width;
				int frame_height;
				int frames_count;
				gfx::color color;
		};
		std::list<Batch> _batch;
		TLFX::AnimImage *_lastSprite = nullptr;
		bool _lastAdditive;
		SDL_Renderer* renderer = nullptr;
};

class SDL2Image: public TLFX::AnimImage {
	public:
		SDL2Image();
		~SDL2Image();

		bool Load(SDL_Renderer* ren, std::string filename) override;
		SDL_Texture* GetTexture() const;

	protected:
		SDL_Texture* _texture = nullptr;
};

#endif // _MARMALADEEFFECTSLIBRARY_H
