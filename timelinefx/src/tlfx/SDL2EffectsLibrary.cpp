#include <SDL2/SDL.h>
#include <SDL2/SDL_stdinc.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_image.h>
#include <cassert>
#include <cmath>
#include <vector>
#include <iostream>

#include "../gfx/gfx.h"

#include "../sdl/SDL2_gfxPrimitives.h"
#include "SDL2EffectsLibrary.h"

#include "TLFXPugiXMLLoader.h"

TLFX::XMLLoader* SDL2EffectsLibrary::CreateLoader() const {
	return new TLFX::PugiXMLLoader(0);
}

TLFX::AnimImage* SDL2EffectsLibrary::CreateImage() const {
	return new SDL2Image();
}

bool SDL2Image::Load(SDL_Renderer* ren, std::string filename) {

	_texture = gfx::load_texture(ren, filename);
	int w, h;
	SDL_QueryTexture(_texture, nullptr, nullptr, &w, &h);

	this->_width = w;
	this->_height = h;

	// how to detect failure?
	return true;
}

SDL2Image::SDL2Image() :
		_texture(nullptr) {

}

SDL2Image::~SDL2Image() {
	if (_texture)
		SDL_DestroyTexture(_texture);
}

SDL_Texture* SDL2Image::GetTexture() const {
	return _texture;
}

SDL2ParticleManager::SDL2ParticleManager(SDL_Renderer* ren, int particles /*= particleLimit*/, int layers /*= 1*/) :
		TLFX::ParticleManager(particles, layers), _lastSprite(NULL), _lastAdditive(true), renderer(ren) {

}

void SDL2ParticleManager::DrawSprite( //
		TLFX::AnimImage* sprite, //
		float px, //
		float py, //
		float frame, //
		float x, //
		float y, //
		float rotation, //
		float scaleX, //
		float scaleY, //
		unsigned char r, //
		unsigned char g, //
		unsigned char b, //
		float a, //
		bool additive //
		) {

	unsigned char alpha = (unsigned char) (a * 255);

	if (alpha == 0 || scaleX == 0 || scaleY == 0)
		return;

	if (sprite != _lastSprite || additive != _lastAdditive)
		Flush();

	Batch batch;

	batch.frame_width = sprite->get_frame_width();
	batch.frame_height = sprite->get_frame_height();
	batch.frames_count = sprite->GetFramesCount();
	batch.px = px;
	batch.py = py;
	batch.frame = frame;
	batch.x = x;
	batch.y = y;
	batch.rotation = rotation;
	batch.scaleX = scaleX;
	batch.scaleY = scaleY;
	batch.color.set(r, g, b, alpha);
	_batch.push_back(batch);

	_lastSprite = sprite;
	_lastAdditive = additive;
/*
	std::cout << "-------------- DrawSprite: " << sprite->GetFilename() << std::endl;

	std::cout << "\tbatch.px: " << batch.px << std::endl;
	std::cout << "\tbatch.py: " << batch.py << std::endl;

	std::cout << "\tbatch.frame: " << batch.frame << std::endl;
	std::cout << "\tbatch.frames: " << batch.frames_count << std::endl;
	std::cout << "\tbatch.frame_width: " << batch.frame_width << std::endl;
	std::cout << "\tbatch.frame_height: " << batch.frame_height << std::endl;

	std::cout << "\tbatch.x: " << batch.x << std::endl;
	std::cout << "\tbatch.y: " << batch.y << std::endl;

	std::cout << "\tbatch.rotation: " << batch.rotation << std::endl;

	std::cout << "\tbatch.scaleX: " << batch.scaleX << std::endl;
	std::cout << "\tbatch.scaleY: " << batch.scaleY << std::endl;

	std::cout << "\tbatch.color: " << std::to_string(batch.color.get_green()) << ", " << std::to_string(batch.color.get_red()) << ", " << std::to_string(batch.color.get_blue()) << ", " << std::to_string(batch.color.get_alpha()) << std::endl;
*/
}

void SDL2ParticleManager::Flush() {

	SDL_Rect dest;
	SDL_Rect src;

	if (!_batch.empty() && _lastSprite) {

		for (auto& b : _batch) {

			dest.x = b.px;
			dest.y = b.py;
			dest.w = _lastSprite->GetWidth() * b.scaleX;
			dest.h = _lastSprite->GetHeight() * b.scaleY;

			if ((int) b.frame <= 1) {

				src.x = 0;
				src.y = 0;
				src.w = b.frame_width;
				src.h = b.frame_height;

			} else {

				const int tiles_in_column = _lastSprite->GetHeight() / b.frame_height;
				const int tiles_in_row = _lastSprite->GetWidth() / b.frame_width;


				const int tile_y = floor(floor(b.frame)/tiles_in_column)*b.frame_height;
				src.y = tile_y;

				const int tile_x = floor(fmod(floor(b.frame), tiles_in_row) * b.frame_width);
						// floor(fmod(b.frame, tiles_in_row) * b.frame_width);
				src.x = tile_x;

				src.w = b.frame_width;
				src.h = b.frame_height;

			}

			SDL_SetTextureAlphaMod(static_cast<SDL2Image*>(_lastSprite)->GetTexture(), //
					b.color.get_alpha() //
					);

			SDL_SetTextureColorMod(static_cast<SDL2Image*>(_lastSprite)->GetTexture(), //
					b.color.get_red(), //
					b.color.get_green(), //
					b.color.get_blue()); //

			SDL_RenderCopyEx( //
					renderer, //
					static_cast<SDL2Image*>(_lastSprite)->GetTexture(), //
					&src, //
					&dest, //
					b.rotation, //
					nullptr, //
					SDL_RendererFlip::SDL_FLIP_NONE //
					);

			/*
			 */
//            SDL_SetRenderDrawColor(renderer, colors[0].get_red(), colors[1].get_green(), colors[2].get_blue(), colors[3].get_alpha());
			//SDL_SetTextureColorMod(static_cast<SDL2Image*>(_lastSprite)->GetTexture(), colors[0].get_red(), colors[0].get_green(), colors[0].get_blue());
//			SDL_RenderCopyEx(renderer, static_cast<SDL2Image*>(_lastSprite)->GetTexture(), nullptr, &dest, it->rotation, nullptr, SDL_RendererFlip::SDL_FLIP_NONE);
		}

		_batch.clear();
	}
}
