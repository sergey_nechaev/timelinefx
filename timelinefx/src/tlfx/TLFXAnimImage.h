#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TLFX_ANIMIMAGE_H
#define _TLFX_ANIMIMAGE_H

#include <string>

struct SDL_Renderer;

namespace TLFX {

	class AnimImage {
		public:
			AnimImage();
			virtual ~AnimImage() {
			}

			virtual bool Load(SDL_Renderer* ren, std::string filename) = 0;

			void SetWidth(float width);
			virtual float GetWidth() const;
			void SetHeight(float height);
			virtual float GetHeight() const;
			void SetMaxRadius(float radius);
			virtual float GetMaxRadius() const;
			void SetFramesCount(int frames);
			virtual int GetFramesCount() const;
			void SetIndex(int index);
			virtual int GetIndex() const;
			void SetFilename(const char *filename);
			virtual const char *GetFilename() const;
			void SetName(const char *name);
			virtual const char *GetName() const;

			virtual void FindRadius() {
			}

			float get_frame_height() const;
			void set_frame_height(float frame_height = 0);
			float get_frame_width() const;
			void set_frame_width(float frame_width = 0);

		protected:
			float _width;
			float _height;
			float _maxRadius;
			int _index;
			int _frames;
			std::string _filename;
			std::string _name;

			float frame_width = 0;
			float frame_height = 0;

	};

} // namespace TLFX

#endif // _TLFX_ANIMIMAGE_H
