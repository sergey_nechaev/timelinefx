sdl2 timelinefx
===============

[TimelineFX](http://www.rigzsoft.co.uk/) particle system in C++ with SDL2 as sample.

Built on top of [Marmalade port] (https://github.com/damucz/timelinefx)
